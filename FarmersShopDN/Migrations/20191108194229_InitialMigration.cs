﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FarmersShopDN.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Administrators",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CretionDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Administrators", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BuyerCategories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CretionDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    BuyerCategoryName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuyerCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductCategories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CretionDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    ProductCategoryName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sellers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CretionDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    Nickname = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Adress = table.Column<string>(nullable: true),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sellers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Buyers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CretionDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    Nickname = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Adress = table.Column<string>(nullable: true),
                    Rating = table.Column<int>(nullable: false),
                    BuyerCategoryId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Buyers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Buyers_BuyerCategories_BuyerCategoryId",
                        column: x => x.BuyerCategoryId,
                        principalTable: "BuyerCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CretionDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    ProductName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    SellerId = table.Column<Guid>(nullable: true),
                    ProductCategoryId = table.Column<Guid>(nullable: true),
                    PricePerKg = table.Column<int>(nullable: false),
                    Kg = table.Column<int>(nullable: false),
                    Discount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_ProductCategories_ProductCategoryId",
                        column: x => x.ProductCategoryId,
                        principalTable: "ProductCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Products_Sellers_SellerId",
                        column: x => x.SellerId,
                        principalTable: "Sellers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BuyersSubscriptions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CretionDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    BuyerId = table.Column<Guid>(nullable: true),
                    ProductCategoryId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuyersSubscriptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BuyersSubscriptions_Buyers_BuyerId",
                        column: x => x.BuyerId,
                        principalTable: "Buyers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BuyersSubscriptions_ProductCategories_ProductCategoryId",
                        column: x => x.ProductCategoryId,
                        principalTable: "ProductCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OtherInfos",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CretionDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    PublisherBuyerId = table.Column<Guid>(nullable: true),
                    PublisherSellerId = table.Column<Guid>(nullable: true),
                    AdministratorId = table.Column<Guid>(nullable: true),
                    IsAcceptable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OtherInfos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OtherInfos_Administrators_AdministratorId",
                        column: x => x.AdministratorId,
                        principalTable: "Administrators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OtherInfos_Buyers_PublisherBuyerId",
                        column: x => x.PublisherBuyerId,
                        principalTable: "Buyers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OtherInfos_Sellers_PublisherSellerId",
                        column: x => x.PublisherSellerId,
                        principalTable: "Sellers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Ratings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CretionDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    FromSellerId = table.Column<Guid>(nullable: true),
                    FromBuyerId = table.Column<Guid>(nullable: true),
                    ToSellerId = table.Column<Guid>(nullable: true),
                    ToBuyerId = table.Column<Guid>(nullable: true),
                    Comment = table.Column<string>(nullable: true),
                    Grade = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ratings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Ratings_Buyers_FromBuyerId",
                        column: x => x.FromBuyerId,
                        principalTable: "Buyers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ratings_Sellers_FromSellerId",
                        column: x => x.FromSellerId,
                        principalTable: "Sellers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ratings_Buyers_ToBuyerId",
                        column: x => x.ToBuyerId,
                        principalTable: "Buyers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ratings_Sellers_ToSellerId",
                        column: x => x.ToSellerId,
                        principalTable: "Sellers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Relations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CretionDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    FirstAdministratorId = table.Column<Guid>(nullable: true),
                    SecondAdministratorId = table.Column<Guid>(nullable: true),
                    FirstSellerId = table.Column<Guid>(nullable: true),
                    SecondSellerId = table.Column<Guid>(nullable: true),
                    FirstBuyerId = table.Column<Guid>(nullable: true),
                    SecondBuyerId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Relations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Relations_Administrators_FirstAdministratorId",
                        column: x => x.FirstAdministratorId,
                        principalTable: "Administrators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Relations_Buyers_FirstBuyerId",
                        column: x => x.FirstBuyerId,
                        principalTable: "Buyers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Relations_Sellers_FirstSellerId",
                        column: x => x.FirstSellerId,
                        principalTable: "Sellers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Relations_Administrators_SecondAdministratorId",
                        column: x => x.SecondAdministratorId,
                        principalTable: "Administrators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Relations_Buyers_SecondBuyerId",
                        column: x => x.SecondBuyerId,
                        principalTable: "Buyers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Relations_Sellers_SecondSellerId",
                        column: x => x.SecondSellerId,
                        principalTable: "Sellers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CretionDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    BuyerId = table.Column<Guid>(nullable: true),
                    ProductId = table.Column<Guid>(nullable: true),
                    Kgs = table.Column<int>(nullable: false),
                    AdministratorId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Administrators_AdministratorId",
                        column: x => x.AdministratorId,
                        principalTable: "Administrators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Buyers_BuyerId",
                        column: x => x.BuyerId,
                        principalTable: "Buyers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OtherInfoSubscriptions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CretionDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    OtherInfoId = table.Column<Guid>(nullable: true),
                    SubscriberBuyerId = table.Column<Guid>(nullable: true),
                    SubscriberSellerId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OtherInfoSubscriptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OtherInfoSubscriptions_OtherInfos_OtherInfoId",
                        column: x => x.OtherInfoId,
                        principalTable: "OtherInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OtherInfoSubscriptions_Buyers_SubscriberBuyerId",
                        column: x => x.SubscriberBuyerId,
                        principalTable: "Buyers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OtherInfoSubscriptions_Sellers_SubscriberSellerId",
                        column: x => x.SubscriberSellerId,
                        principalTable: "Sellers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Chats",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CretionDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    RelationId = table.Column<Guid>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    OtherInfoId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chats", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Chats_OtherInfos_OtherInfoId",
                        column: x => x.OtherInfoId,
                        principalTable: "OtherInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Chats_Relations_RelationId",
                        column: x => x.RelationId,
                        principalTable: "Relations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Buyers_BuyerCategoryId",
                table: "Buyers",
                column: "BuyerCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_BuyersSubscriptions_BuyerId",
                table: "BuyersSubscriptions",
                column: "BuyerId");

            migrationBuilder.CreateIndex(
                name: "IX_BuyersSubscriptions_ProductCategoryId",
                table: "BuyersSubscriptions",
                column: "ProductCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Chats_OtherInfoId",
                table: "Chats",
                column: "OtherInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_Chats_RelationId",
                table: "Chats",
                column: "RelationId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_AdministratorId",
                table: "Orders",
                column: "AdministratorId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_BuyerId",
                table: "Orders",
                column: "BuyerId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ProductId",
                table: "Orders",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_OtherInfos_AdministratorId",
                table: "OtherInfos",
                column: "AdministratorId");

            migrationBuilder.CreateIndex(
                name: "IX_OtherInfos_PublisherBuyerId",
                table: "OtherInfos",
                column: "PublisherBuyerId");

            migrationBuilder.CreateIndex(
                name: "IX_OtherInfos_PublisherSellerId",
                table: "OtherInfos",
                column: "PublisherSellerId");

            migrationBuilder.CreateIndex(
                name: "IX_OtherInfoSubscriptions_OtherInfoId",
                table: "OtherInfoSubscriptions",
                column: "OtherInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_OtherInfoSubscriptions_SubscriberBuyerId",
                table: "OtherInfoSubscriptions",
                column: "SubscriberBuyerId");

            migrationBuilder.CreateIndex(
                name: "IX_OtherInfoSubscriptions_SubscriberSellerId",
                table: "OtherInfoSubscriptions",
                column: "SubscriberSellerId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ProductCategoryId",
                table: "Products",
                column: "ProductCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_SellerId",
                table: "Products",
                column: "SellerId");

            migrationBuilder.CreateIndex(
                name: "IX_Ratings_FromBuyerId",
                table: "Ratings",
                column: "FromBuyerId");

            migrationBuilder.CreateIndex(
                name: "IX_Ratings_FromSellerId",
                table: "Ratings",
                column: "FromSellerId");

            migrationBuilder.CreateIndex(
                name: "IX_Ratings_ToBuyerId",
                table: "Ratings",
                column: "ToBuyerId");

            migrationBuilder.CreateIndex(
                name: "IX_Ratings_ToSellerId",
                table: "Ratings",
                column: "ToSellerId");

            migrationBuilder.CreateIndex(
                name: "IX_Relations_FirstAdministratorId",
                table: "Relations",
                column: "FirstAdministratorId");

            migrationBuilder.CreateIndex(
                name: "IX_Relations_FirstBuyerId",
                table: "Relations",
                column: "FirstBuyerId");

            migrationBuilder.CreateIndex(
                name: "IX_Relations_FirstSellerId",
                table: "Relations",
                column: "FirstSellerId");

            migrationBuilder.CreateIndex(
                name: "IX_Relations_SecondAdministratorId",
                table: "Relations",
                column: "SecondAdministratorId");

            migrationBuilder.CreateIndex(
                name: "IX_Relations_SecondBuyerId",
                table: "Relations",
                column: "SecondBuyerId");

            migrationBuilder.CreateIndex(
                name: "IX_Relations_SecondSellerId",
                table: "Relations",
                column: "SecondSellerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BuyersSubscriptions");

            migrationBuilder.DropTable(
                name: "Chats");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "OtherInfoSubscriptions");

            migrationBuilder.DropTable(
                name: "Ratings");

            migrationBuilder.DropTable(
                name: "Relations");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "OtherInfos");

            migrationBuilder.DropTable(
                name: "ProductCategories");

            migrationBuilder.DropTable(
                name: "Administrators");

            migrationBuilder.DropTable(
                name: "Buyers");

            migrationBuilder.DropTable(
                name: "Sellers");

            migrationBuilder.DropTable(
                name: "BuyerCategories");
        }
    }
}
