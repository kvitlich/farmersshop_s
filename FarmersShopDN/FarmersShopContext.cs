﻿using FarmersShopDN.Domain;
using Microsoft.EntityFrameworkCore;
using System;

namespace FarmersShopDN
{
    public class FarmersShopContext : DbContext
    {
        public DbSet<Administrator> Administrators { get; set; }
        public DbSet<Buyer> Buyers { get; set; }
        public DbSet<BuyerCategory> BuyerCategories { get; set; }
        public DbSet<BuyersSubscriptions> BuyersSubscriptions { get; set; }
        public DbSet<Chat> Chats { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OtherInfo> OtherInfos { get; set; }
        public DbSet<OtherInfoSubscriptions> OtherInfoSubscriptions { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Relation> Relations { get; set; }
        public DbSet<Seller> Sellers { get; set; }

        public FarmersShopContext()
        {
            Database.Migrate();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=DESKTOP-IM6E3IQ\\SQLDEVELOPER;Database=FarmersShop;Trusted_Connection=True;");
        }
    }
}
