﻿using System;
using System.Collections.Generic;

namespace FarmersShopDN.Domain
{
    public class OtherInfo : Entity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public virtual Buyer PublisherBuyer { get; set; }
        public virtual Seller PublisherSeller { get; set; }
        public virtual Administrator Administrator { get; set; }
        public bool IsAcceptable { get; set; }
        public ICollection<OtherInfo> OtherInfos { get; set; } = new List<OtherInfo>();
    }
}
