﻿using System;
using System.Collections.Generic;

namespace FarmersShopDN.Domain
{
    public class ProductCategory : Entity
    {
        public string ProductCategoryName { get; set; }
        public ICollection<Product> Products { get; set; } = new List<Product>();
        public ICollection<BuyersSubscriptions> BuyersSubscriptions { get; set; } = new List<BuyersSubscriptions>();
    }
}
