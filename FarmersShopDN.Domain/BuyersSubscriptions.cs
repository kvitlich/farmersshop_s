﻿using System;

namespace FarmersShopDN.Domain
{
    public class BuyersSubscriptions : Entity
    {
        public virtual Buyer Buyer { get; set; }
        public virtual ProductCategory ProductCategory { get; set; }
    }
}
