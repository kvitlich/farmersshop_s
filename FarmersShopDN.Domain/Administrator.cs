﻿using System;
using System.Collections.Generic;

namespace FarmersShopDN.Domain
{
    public class Administrator : Entity
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public ICollection<Order> Orders { get; set; } = new List<Order>();
        //public ICollection<Relation> Relations { get; set; } = new List<Relation>();
    }
}
