﻿using System;

namespace FarmersShopDN.Domain
{
    public class Chat : Entity
    {
        public virtual Relation Relation { get; set; }
        public string Message { get; set; }
        public virtual OtherInfo OtherInfo { get; set; }
    }
}
