﻿using System;

namespace FarmersShopDN.Domain
{
    public abstract class Entity
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public DateTime CretionDate { get; set; } = DateTime.Now;
        public DateTime? DeletedDate { get; set; }
    }
}
