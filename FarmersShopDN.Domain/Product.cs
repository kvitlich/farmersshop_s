﻿using System;
using System.Collections.Generic;

namespace FarmersShopDN.Domain
{
    public class Product : Entity
    {
        public string ProductName { get; set; }
        public string Description { get; set; }
        public virtual Seller Seller { get; set; }
        public virtual ProductCategory ProductCategory { get; set; }
        public int PricePerKg { get; set; }
        public int Kg { get; set; }
        public int Discount { get; set; }
        public ICollection<Order> Orders { get; set; } = new List<Order>();
    }
}
