﻿using System;

namespace FarmersShopDN.Domain
{
    public class Order : Entity
    {
        public virtual Buyer Buyer { get; set; }
        public virtual Product Product { get; set; }
        public int Kgs { get; set; }
        public virtual Administrator Administrator { get; set; }
    }
}
