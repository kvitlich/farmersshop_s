﻿using System;

namespace FarmersShopDN.Domain
{
    public class Rating : Entity
    {
        public virtual Seller FromSeller { get; set; }
        public virtual Buyer FromBuyer { get; set; }
        public virtual Seller ToSeller { get; set; }
        public virtual Buyer ToBuyer { get; set; }
        public string Comment { get; set; }
        public int Grade { get; set; }
    }
}
