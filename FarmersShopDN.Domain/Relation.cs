﻿using System;

namespace FarmersShopDN.Domain
{
    public class Relation : Entity
    {
        public virtual Administrator FirstAdministrator { get; set; }
        public virtual Administrator SecondAdministrator { get; set; }
        public virtual Seller FirstSeller { get; set; }
        public virtual Seller SecondSeller { get; set; }
        public virtual Buyer FirstBuyer { get; set; }
        public virtual Buyer SecondBuyer { get; set; }
        //public virtual Chat Chat { get; set; }
    }
}
