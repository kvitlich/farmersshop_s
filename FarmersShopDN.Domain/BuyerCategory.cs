﻿using System;
using System.Collections.Generic;

namespace FarmersShopDN.Domain
{
    public class BuyerCategory : Entity
    {
        public string BuyerCategoryName { get; set; }
        public ICollection<Buyer> Buyers { get; set; } = new List<Buyer>();
    }
}
