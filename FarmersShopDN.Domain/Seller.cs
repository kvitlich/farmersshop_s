﻿using System;
using System.Collections.Generic;

namespace FarmersShopDN.Domain
{
    public class Seller : Entity
    {
        public string Nickname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Adress { get; set; }
        public int Rating { get; set; }
        public ICollection<Product> Products { get; set; } = new List<Product>();
        public ICollection<OtherInfo> OtherInfos { get; set; } = new List<OtherInfo>();
        public ICollection<OtherInfoSubscriptions> OtherInfoSubscriptions { get; set; } = new List<OtherInfoSubscriptions>();
        //public ICollection<Rating> Ratings { get; set; } = new List<Rating>();
        //public ICollection<Relation> Relations { get; set; } = new List<Relation>();
    }
}
